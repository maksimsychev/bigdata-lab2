package com.sychev;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

/**
 *
 * Counts amount of twitter deletions for 60 sec
 * Input format - text file
 *
 */

public final class TwitterDelCount {

    public static void main(String[] args) throws Exception {

        if (args.length < 1) {
            System.err.println("Usage: JavaTwitterDelsCount <file>");
            System.exit(1);
        }

        SparkConf sparkConf = new SparkConf().setAppName("JavaTwitterDelsCount");
        JavaSparkContext ctx = new JavaSparkContext(sparkConf);
        JavaRDD<String> lines = ctx.textFile(args[0], 1);

        JavaRDD<String> dels = getFilteredStringJavaRDD(lines);
        long delsNum = getDelsNum(dels);


        System.out.println("-----------------------");
        System.out.println("Deleted twittes: " + delsNum);
        System.out.println("-----------------------");
        ctx.stop();
    }

    /**
     * Count the number of elements in the RDD
     * @param dels
     * @return Return the number of elements in the RDD
     */
    public  static long getDelsNum(JavaRDD<String> dels) {
        return dels.count();
    }

    /**
     * Filter input JavaRDD: Check if line contains string "\"delete\":".
     * @param lines
     * @return JavaRDD with strings those contains "\"delete\":"
     */
    public  static JavaRDD<String> getFilteredStringJavaRDD(JavaRDD<String> lines) {
        return lines.filter(new Function<String, Boolean>() {
                public Boolean call(String s) { return s.contains("\"delete\":"); }
            });
    }
}

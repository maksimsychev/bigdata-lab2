package com.sychev;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.apache.spark.api.java.JavaRDD;
import org.junit.Test;

import com.holdenkarau.spark.testing.JavaRDDComparisons;
import com.holdenkarau.spark.testing.SharedJavaSparkContext;

public class TwitterDelCountTest extends SharedJavaSparkContext implements Serializable{

    private static final long serialVersionUID = 1L;

    @Test
    public void shouldCountInputFileContent() throws IOException {

        Scanner s = new Scanner(new File("C:\\Users\\pc\\IdeaProjects\\bigData2\\src\\test\\resources\\sample.txt"));
        List<String> inputList = new ArrayList<String>();
        while (s.hasNextLine()){
            inputList.add(s.nextLine());
        }
        s.close();

        JavaRDD<String> countRdd = TwitterDelCount.getFilteredStringJavaRDD(jsc().parallelize(inputList, 1));


        List<String> expectedList = Arrays.asList(
                new String("{\"delete\":{\"status\":{\"id\":968979640223121408,\"id_str\":\"968979640223121408\",\"user_id\":933523604003631104,\"user_id_str\":\"933523604003631104\"},\"timestamp_ms\":\"1544828397910\"}}\n"),
                new String("{\"delete\":{\"status\":{\"id\":1073074795464421377,\"id_str\":\"1073074795464421377\",\"user_id\":1046454851612397569,\"user_id_str\":\"1046454851612397569\"},\"timestamp_ms\":\"1544828398887\"}}\n"),
                new String("{\"delete\":{\"status\":{\"id\":1067207048075595778,\"id_str\":\"1067207048075595778\",\"user_id\":210038916,\"user_id_str\":\"210038916\"},\"timestamp_ms\":\"1544828399021\"}}\n"),
                new String("{\"delete\":{\"status\":{\"id\":1073493873555574784,\"id_str\":\"1073493873555574784\",\"user_id\":800466009987481600,\"user_id_str\":\"800466009987481600\"},\"timestamp_ms\":\"1544828399131\"}}\n"),
                new String("{\"delete\":{\"status\":{\"id\":529743048897740800,\"id_str\":\"529743048897740800\",\"user_id\":438644904,\"user_id_str\":\"438644904\"},\"timestamp_ms\":\"1544828399182\"}}\n")
        );


        JavaRDD<String> parallelizeExpected = jsc().parallelize(expectedList);

        JavaRDDComparisons.assertRDDEquals(parallelizeExpected, countRdd);

    }

}